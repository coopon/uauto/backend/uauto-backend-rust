extern crate uauto_backend_rust as uauto;
extern crate diesel;

use uauto::schema::users;
use diesel::*;
//use uauto::schema::stands:

#[derive(Insertable, Debug)]
#[table_name = "users"]
struct Driver {
    pub mail: String,
    pub number: String,
    pub password: String,
    pub name: String,
    pub stand_id: i64,
    pub u_type: i32,
}

fn main() {

    let connection = uauto::establish_connection();

    use uauto::schema::stands::dsl::*;
    use uauto::schema::users::dsl::*;

    let stand_list = stands.select(node_id).load::<i64>(&connection).unwrap();

    let mut user: Driver;
    let mut phone_number = 9999999999i64;
    let mut driver_number = 1;
    let mut user_list = vec![];

    for i in stand_list{
        //println!("{:?}",i);
        for _k in 1..16{
        user = Driver{
                name: format!("Driver {}",driver_number),
                mail: format!("driver{}@uauto.com",driver_number),
                number: format!("{}",phone_number),
                stand_id: i,
                u_type: 1,
                password: "password".to_string(),
            };
        phone_number-=1;
        driver_number+=1;
        user_list.push(user);
    }
    }
    insert_into(users).values(&user_list).execute(&connection).unwrap();
    println!("Pushed data into db");
}
