extern crate osm_xml as osm;
extern crate uauto_backend_rust as uauto;
extern crate diesel;

use diesel::*;
use std::fs::File;
use uauto::models;
//use uauto::schema::stands:


fn main() {
    let f = File::open("stands.xml").unwrap();
    let doc = osm::OSM::parse(f).unwrap();

    let connection = uauto::establish_connection();

    let mut stand_list = vec![];

    let mut stand: models::Stand;

    use uauto::schema::stands::dsl::*;

    let mut stand_name = "".to_string();

    for i in &doc.nodes{
        for k in &i.1.tags{
            if k.key == "name" {
               stand_name = k.val.to_string();
                break;
            } else {
                stand_name = "".to_string();
            }
        };

        stand = models::Stand{
            node_id: i.1.id as i64,
            lat: i.1.lat.to_string(),
            lon: i.1.lon.to_string(),
            name: Some(stand_name.to_string())
        };

        println!("{:?}",stand);
        stand_list.push(stand);
    }
        insert_into(stands).values(&stand_list).execute(&connection).unwrap();
        println!("Pushed stand data into db");

  // println!("Node count {:?}", doc.nodes.values());

}
