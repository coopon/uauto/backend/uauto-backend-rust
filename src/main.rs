extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
extern crate actix;
extern crate actix_web;
extern crate env_logger;
extern crate futures;
extern crate r2d2;
//extern crate uuid;

use actix::prelude::*;
use actix_web::{
    http, middleware, server, App};

use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;

mod db;
mod models;
mod schema;
mod api;

use websocket::*;

use db::DbExecutor;

/// State with DbExecutor address
pub struct AppState {
    db: Addr<DbExecutor>,
}
fn main() {
    // Set up the logger
    ::std::env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();
    let sys = actix::System::new("uauto_backend_rust");

    // connect to postgres
    let manager = ConnectionManager::<PgConnection>::new("postgres://demonshreder:password@localhost/test");
    // Build r2d2 pool
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");
    // Activate r2d2 pool via 3 sync actors
    let addr = SyncArbiter::start(3, move || DbExecutor(pool.clone()));

    // Start http server
    server::new(move || {
        App::with_state(AppState{db: addr.clone()})
            // enable logger
            .middleware(middleware::Logger::default())
            // routes here
            .resource("/users", |r| r.method(http::Method::POST).with(api::users_post))
        //.unwrap()
        .start();
    println!("Started http server: 127.0.0.1:8080");
    let _ = sys.run();
}
