extern crate serde;
extern crate serde_json;
extern crate actix;
extern crate actix_web;
extern crate env_logger;
extern crate futures;
//extern crate uuid;

use actix_web::{
    AsyncResponder,Error, HttpResponse, State, Json};

use futures::Future;

use models;

/// Handler for /users POST - creates new user and returns it
pub fn users_post (
    (user, state): (Json<models::NewUser>, State<super::AppState>),
) -> Box<Future<Item=HttpResponse, Error=Error>> {
    // Try to make this zero copy?
    state
        .db
        .send(models::NewUser{
            mail: user.mail.to_owned(),
            name: user.name.to_owned(),
            password: user.password.to_owned(),
            u_type: user.u_type.to_owned(),
            number: user.number.to_owned()
        })
        .from_err()
        .and_then(|res| match res {
            Ok(user) => Ok(HttpResponse::Ok().json(user)),
            Err(_) => Ok(HttpResponse::InternalServerError().into()),
        })
        .responder()
}
