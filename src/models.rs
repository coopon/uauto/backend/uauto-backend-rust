extern crate actix;

use schema::*;
//use models::actix::prelude::*;
//use diesel::result::Error;
#[derive(Serialize, Deserialize, Queryable, Insertable, Debug)]
#[table_name = "stands"]
pub struct Stand {
    pub node_id: i64,
    pub lat: String,
    pub lon: String,
    pub name: Option<String>
}


#[derive(Serialize, Deserialize, Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser {
    pub mail: Option<String>,
    pub number: Option<String>,
    pub name: Option<String>,
    pub password: String,
    pub u_type: i32,
}

#[derive(Serialize, Deserialize, Identifiable, Queryable, Debug)]
//#[table_name = "users"]

pub struct User {
    pub mail: Option<String>,
    pub number: Option<String>,
    pub id: i32,
    pub name: Option<String>,
    pub password: String,
    pub stand_id: Option<i64>,
    pub u_type: i32,
}
