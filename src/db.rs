//! Db executor actor
use actix::prelude::*;
use actix_web::*;
use diesel;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};
//use uuid;

use models;
use models::NewUser;
use schema;

/// This is db executor actor. We are going to run 3 of them in parallel.
pub struct DbExecutor(pub Pool<ConnectionManager<PgConnection>>);

//impl diesel::Connection for DbExecutor {}

/// This is only message that this actor can handle, but it is easy to extend
/// number of messages.

impl Message for NewUser {
    type Result = Result<models::User, Error>;
}

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

impl Handler<NewUser> for DbExecutor {
    type Result = Result<models::User, Error>;

    fn handle(&mut self, msg: NewUser, _: &mut Self::Context) -> Self::Result {
        use self::schema::users::dsl::*;

        let new_user = models::NewUser {
            mail: msg.mail,
            name: msg.name,
            password: msg.password,
            u_type: msg.u_type,
            number: msg.number
        };
       let conn: &PgConnection = &self.0.get().unwrap();

        diesel::insert_into(users)
            .values(&new_user)
            .execute(conn)
            .map_err(|_| error::ErrorInternalServerError("Error inserting person"))?;

        let mut items = users
            .filter(number.eq(new_user.number))
           .load::<models::User>(conn)
           .map_err(|_| error::ErrorInternalServerError("Error loading person"))?;

        Ok(items.pop().unwrap())
    }
}
