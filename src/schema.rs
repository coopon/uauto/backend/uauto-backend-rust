table! {
    stands (node_id) {
        node_id -> Int8,
        name -> Nullable<Text>,
        lat -> Text,
        lon -> Text,
    }
}

table! {
    users (id) {
        mail -> Nullable<Text>,
        number -> Nullable<Text>,
        id -> Int4,
        name -> Nullable<Text>,
        password -> Text,
        stand_id -> Nullable<Int8>,
        u_type -> Int4,
    }
}

joinable!(users -> stands (stand_id));

allow_tables_to_appear_in_same_query!(
    stands,
    users,
);
