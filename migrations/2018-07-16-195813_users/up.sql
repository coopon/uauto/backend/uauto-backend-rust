CREATE TABLE users (
mail TEXT,
number TEXT,
id SERIAL PRIMARY KEY,
name TEXT,
password TEXT NOT NULL,
stand_id BIGINT references stands(node_id),
u_type INT NOT NULL
)
